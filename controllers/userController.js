const models = require('../models/index');
const multer = require('multer');
const path = require('path');
const crypto = require('crypto');

/**
 * Untuk set direktori public jadi static assets
 */
const uploadDir = '/images/';
const storage = multer.diskStorage({
  destination: "./public" + uploadDir,
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(4, function (err, raw) {
      if(err) return cb(err)

      cb(null, raw.toString('hex') + '_' + file.originalname)
    });
  }
});

exports.upload = multer({storage: storage, dest: uploadDir});

/**
 * Fungsi untuk membuat user baru
 */
exports.createData = async (req, res, next) => {
    try {
        const {
          nama,
          tanggal_lahir,
          deskripsi
        } = req.body;
    
        const user = await models.users.create({
          nama,
          tanggal_lahir,
          foto: req.file === undefined ? "" : req.file.filename,
          deskripsi
        });
    
        if (user) {
          res.status(201).json({
            'status': 'OK',
            'messages': 'User Berhasil Ditambahkan',
            'data': user
          });
        }
    } catch(err) {
        res.status(400).json({
          'status': 'ERROR',
          'messages': err.messages
        });
    }
}

/**
 * Fungsi untuk mengambil semua data User
 */
exports.readAllData = async (req, res, next) => {
    try {
        const users = await models.users.findAll({});
    
        if (users.length !== 0) {
          res.json({
            'status': 'OK',
            'messages': 'All User Data',
            'data': users
          });
        } else {
          res.json({
            'status': 'EMPTY',
            'messages': 'Data is empty',
            'data': {} 
          });
        }
    } catch (err) {
        res.status(500).json({
          'status': 'ERROR',
          'messages': 'Internal Server Error'
        })
    }
}

/**
 * Fungsi untuk mengambil data satu User berdasarkan ID
 */
exports.readOneData = async (req, res, next) => {
    try {
        const id = req.params.id;
        const user = await models.users.findByPk(id);
    
        if (user) {
          res.json({
            'status': 'OK',
            'messages': 'One User Data',
            'data': user
          });
        } else {
          res.status(404).json({
            'status': 'NOT_FOUND',
            'messages': 'Data not found',
            'data': null 
          });
        }
    } catch (err) {
        res.status(500).json({
          'status': 'ERROR',
          'messages': 'Internal Server Error'
        })
    }
}

/**
 * Fungsi untuk mengupdate data User berdasarkan ID
 */
exports.updateData = async (req, res, next) => {
    try {
        const id = req.params.id
        const {
          nama,
          tanggal_lahir,
          deskripsi
        } = req.body;
    
        const user = models.users.update({
          nama,
          tanggal_lahir,
          foto: req.file === undefined ? "" : req.file.filename,
          deskripsi
        }, {
          where: {
            id: id
          }
        });
    
        if (user) {
          res.json({
            'status': 'OK',
            'messages': 'User Berhasil Diubah'
          });
        }
    } catch {
        res.status(400).json({
          'status': 'ERROR',
          'messages': err.message
        })
    }
}

/**
 * Fungsi untuk menghapus User berdasarkan ID
 */
exports.deleteData = (req, res, next) => {
    try {
        const id = req.params.id
        const user = models.users.destroy({
          where: {
            id: id
          }
        });
    
        if (user) {
          res.json({
            'status': 'OK',
            'messages': 'User Berhasi Dihapus'
          });
        }
    
    } catch(err) {
        res.status(400).json({
          'status': 'ERROR',
          'messages': err.message
        });
    }
}