var express = require('express');
var router = express.Router();
const {
  createData,
  readAllData,
  readOneData,
  updateData,
  deleteData,
  upload
} = require('../controllers/userController');

// Route untuk mengambil semua data User
router.get('/', readAllData);

// Route untuk mengambil data User berdasarkan ID
router.get('/:id', readOneData);

// Route untuk membuat user baru
router.post('/', [upload.single('foto')], createData);

// Route untuk mengupdate data User berdasarkan ID
router.put('/:id', [upload.single('foto')], updateData);

// Route untuk menghapus User berdasarkan ID
router.delete('/:id', deleteData);

module.exports = router;
