# Rest API CRUD Database - Node JS

> Sebuah Projek berisikan Rest API CRUD Database dengan Upload Image. 
>
> Dibuat dengan Node JS + Express JS.

<br>

Setelah Clone Repository ini, jangan lupa untuk :

- Install library yang diperlukan
   
   ```language
   npm install
   ```
