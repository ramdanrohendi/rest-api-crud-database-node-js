'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class table_user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  table_user.init({
    nama: DataTypes.STRING,
    tanggal_lahir: DataTypes.DATE,
    foto: DataTypes.STRING,
    deskripsi: DataTypes.TEXT
  }, {
    sequelize,
    timestamps: true,
    tableName: 'table_users',
    modelName: 'users',
  });
  return table_user;
};